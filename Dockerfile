####################################################################################
# Dockerfile for local
# build stage
# FROM maven:3.5-jdk-8 as build-stage

# WORKDIR /tmp

# COPY src /tmp/src

# COPY pom.xml /tmp

# RUN mvn clean install

# production stage
# FROM tomcat:8-jre11 as production-stage

# RUN rm -rf /usr/local/tomcat/webapps/*

# COPY --from=build-stage /tmp/target/webapp.war /usr/local/tomcat/webapps/ROOT.war

# EXPOSE 8080

# CMD ["catalina.sh", "run"]

####################################################################################
# Dockerfile for CICD
FROM tomcat:8-jre11

RUN rm -rf /usr/local/tomcat/webapp/*

COPY /target/webapp.war /usr/local/tomcat/webapps/ROOT.war

EXPOSE 8080

CMD ["catalina.sh", "run"]