# Simple Webapp with Java
source: https://github.com/devopshydclub/vprofile-project

## Image Information
* Maven: maven:3.5-jdk-8
* Tomcat: tomcat:8-jre11
* Mysql: mysql:5.7
* Rabbitmq: rabbitmq:latest
* Memcached: memcached:latest

## Quickstart
* Build image
    ```
    $ docker built -t registry.gitlab.com/lamkhoi1304/webapp:latest .
    ```
* Run docker-compose
    ```
    docker-compose up -d
    ```